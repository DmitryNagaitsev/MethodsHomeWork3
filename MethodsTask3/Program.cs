﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MethodsTask3
{
    public class Program
    {
        const double XLeft = 0;
        const double XRight = Math.PI;
        const double InitT = 0;
        const double FinalT = 8;
        const double StepX = Math.PI / 2048;
        const double StepT = 0.001;
        private const double EpsNewton = 0.00001;

        public static double InitialConditions(double x, double initialT = 0)
        {
            return Math.Sin(x);
        }

        public static double LeftBoundary(double t, double xLeft = 0.0)
        {
            return 0;
        }

        public static double RightBoundary(double t, double xRight = 8)
        {
            return 0;
        }

        public static double DifferenceOperator(double u)
        {
            return Math.Exp(u);
        }

        public static double DifferenceOperatorDerivative(double u)
        {
            return Math.Exp(u);
        }

        public static double HeterogeniousFunction(double x, double t)
        {
            return -Math.Exp(Math.Sin(x) * Math.Cos(t)) * Math.Sin(x) * Math.Sin(t) 
                + Math.Sin(x) * Math.Cos(t);
        }

        public static double ExactSolution(double x, double t)
        {
            return Math.Sin(x) * Math.Cos(t);
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public static void CalculateCrankNicolson()
        {
            var network = new Network(new Axis(XLeft, XRight, StepX), new Axis(InitT, FinalT, StepT))
                .InitSpace(InitialConditions)
                .InitBorder(LeftBoundary, RightBoundary);

            var Ai = StepT / (2 * StepX * StepX);
            var Ci = StepT / (2 * StepX * StepX);
            var alpha = new double[network.Space.Segments + 1];
            var beta = new double[network.Space.Segments + 1];
            var yk1 = new double[network.Space.Segments + 1];
            var error = 0.0;

            for (var j = 0; j < network.Time.Segments; j++)
            {
                var flag = true;
                var yk = network.GetLayer(j);
                alpha[0] = 0;
                beta[0] = LeftBoundary(network.Time.Coordinate(j + 1));
                yk1[network.Space.Segments] = RightBoundary(network.Time.Coordinate(j + 1));
                while (flag)
                {
                    for (var i = 1; i < network.Space.Segments; i++)
                    {
                        var Fi =
                            -(DifferenceOperator(yk[i - 1]) - DifferenceOperator(network.Solution(i - 1, j)) -
                              DifferenceOperatorDerivative(yk[i - 1]) * yk[i - 1] -
                              StepT * HeterogeniousFunction(network.Space.Coordinate(i - 1),
                                  network.Time.Coordinate(j))) +
                        StepT / (2 * StepX * StepX) * (yk[i + 1] - 2 * yk[i] + yk[i - 1]);
                        var Bi = DifferenceOperatorDerivative(yk[i - 1]) + StepT / (StepX * StepX);
                        alpha[i] = Ci / (Bi - alpha[i - 1] * Ai);
                        beta[i] = (Ai * beta[i - 1] + Fi) / (Bi - alpha[i - 1] * Ai);
                    }
                    //for (var i = 0; i < network.Space.Segments; i++)
                    //{
                    //    var Fi =
                    //        -(DifferenceOperator(yk[i]) - DifferenceOperator(network.Solution(i, j)) -
                    //          DifferenceOperatorDerivative(yk[i]) * yk[i] -
                    //          StepT * HeterogeniousFunction(network.Space.Coordinate(i), network.Time.Coordinate(j)));//+
                    //                                                                                                  //StepT / (2 * StepX * StepX) *
                    //                                                                                                  //(network.Solution(i + 1, j) + network.Solution(i, j) + network.Solution(i - 1, j));
                    //    var Bi = DifferenceOperatorDerivative(yk[i]) + 2 * StepT / (StepX * StepX);
                    //    alpha[i + 1] = Ci / (Bi - alpha[i] * Ai);
                    //    beta[i + 1] = (Ai * beta[i] + Fi) / (Bi - alpha[i] * Ai);
                    //}
                    for (var i = network.Space.Segments - 1; i > 0; i--)
                    {
                        yk1[i] = alpha[i+1] * yk1[i + 1] + beta[i+1];
                    }
                     flag = Norm(yk1, yk) > EpsNewton;
                    yk = yk1;
                }
                network.SetLayer(j + 1, yk1);

                for (var i = 0; i < network.Space.Segments + 1; i++)
                {
                    var dif = Math.Abs(network.Solution(i, j + 1) -
                                       ExactSolution(network.Space.Coordinate(i), network.Time.Coordinate(j + 1)));
                    if (dif > error)
                    {
                        error = dif;
                    }
                }                
            }
            Console.WriteLine(error);
            Console.ReadKey();
        }

        public static double Norm(double[] a, double[] b)
        {
            return a.Select((t, i) => Math.Abs(t - b[i])).Concat(new[] {0.0}).Max();
        }

        public static void Main()
        {
            CalculateCrankNicolson();
        }
    }


    public class Network
    {
        public Axis Space { get; }
        public Axis Time { get; }
        private readonly double[,] _solution;

        public Network(Axis space, Axis time)
        {
            Space = space;
            Time = time;
            _solution = new double[Space.Segments + 1, Time.Segments + 1];
        }

        public double Solution(int x, int t) => _solution[x, t];

        public double[] GetLayer(int layer)
        {
            var target = new double[Space.Segments + 1];
            for (var i = 0; i < target.Length; i++)
            {
                target[i] = Solution(i, layer);
            }
            return target;
        }

        public void SetLayer(int layer, double[] source)
        {
            for (var i = 0; i < source.Length; i++)
            {
                _solution[i, layer] = source[i];
            }
        }

        public Network InitSpace(Func<double, double, double> initFunction)
        {
            for (var i = 0; i <= Space.Segments; i++)
            {
                _solution[i, 0] = initFunction(Space.Coordinate(i), 0);
            }
            return this;
        }

        public Network InitBorder(
            Func<double, double, double> initFunctionLeft,
            Func<double, double, double> initFunctionRight)
        {
            for (var i = 0; i <= Time.Segments; i++)
            {
                _solution[0, i] = initFunctionLeft(Time.Coordinate(i), 0);
                _solution[Space.Segments, i] = initFunctionLeft(Time.Coordinate(i), Space.Right);
            }
            return this;
        }
    }

    public class Axis
    {
        private readonly double _step;

        public Axis(double left, double right, double step)
        {
            Left = left;
            Right = right;
            _step = step;
        }

        public double Left { get; }
        public double Right { get; }

        public double Length => Right - Left;
        public int Segments => (int) (Length / _step);
        public double Coordinate(int i) => i * _step;
    }
}
